//
//  AuthRequestFactory.swift
//  LSShop
//
//  Created by Станислав Лемешаев on 05/08/2019.
//  Copyright © 2019 Станислав Лемешаев. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthRequestFactory {
    func login(userName: String, password: String, completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
}
