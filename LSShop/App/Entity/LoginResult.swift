//
//  LoginResult.swift
//  LSShop
//
//  Created by Станислав Лемешаев on 05/08/2019.
//  Copyright © 2019 Станислав Лемешаев. All rights reserved.
//

import Foundation
struct LoginResult: Codable {
    let result: Int
    let user: User
}
