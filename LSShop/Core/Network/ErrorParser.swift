//
//  ErrorParser.swift
//  LSShop
//
//  Created by Станислав Лемешаев on 05/08/2019.
//  Copyright © 2019 Станислав Лемешаев. All rights reserved.
//

import Foundation
class ErrorParser: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return result
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
